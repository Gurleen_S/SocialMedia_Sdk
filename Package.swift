// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SocialSDK",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SocialSDK",
            targets: ["SocialSDK"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
         .package(url: "https://github.com/facebook/facebook-ios-sdk", .upToNextMajor(from: "9.0.0")),
         .package(url: "https://github.com/google/GoogleSignIn-iOS", .upToNextMajor(from: "7.0.0")),
         .package(url: "https://github.com/ShipBook/ShipBookSDK-iOS.git", from: "1.1.9"),
         .package(url: "https://github.com/Alamofire/Alamofire.git", from:"5.6.4"),
         .package(url: "https://github.com/Alamofire/AlamofireImage.git", from:"4.2.0"),
         .package(url: "https://github.com/Swiftify-Corp/IHProgressHUD.git", from:"0.1.8"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SocialSDK",
            dependencies: [
                .product(name: "GoogleSignIn", package: "GoogleSignIn-iOS"),
                .product(name: "FacebookLogin", package: "facebook-ios-sdk"),
                .product(name: "ShipBookSDK", package: "ShipBookSDK-iOS"),
                .product(name: "Alamofire", package: "Alamofire"),
                .product(name: "IHProgressHUD", package: "IHProgressHUD"),
                .product(name: "AlamofireImage", package: "AlamofireImage")]),
    ]
)

