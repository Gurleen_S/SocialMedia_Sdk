//
//  ApiActions.swift
//  ME Customer
//
//  Created by Gurpreet Singh on 13/03/21.
//

import Foundation
import Alamofire

extension Parameters {
    
}


let ApiActions = _ApiActions();

//-- class to access apis.
class _ApiActions {
    
    /**
     - URL : 'https://www.googleapis.com/auth/photoslibrary'
     - Method : 'GET'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */
    func googleAlbum(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {
        let _headers : HTTPHeaders = [
            KEYS.local.contentType:KEYS.local.applicationJson,
            KEYS.local.authorization : "Bearer \(parameters?[KEYS.apiParams._accessToken] ?? "")"
        ]
        APIManager.shared.get(url: KEYS.url.googlePhotoAlbum,
                              headers: _headers,
                              parameters: nil,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
    
    /**
     - URL : 'https://photoslibrary.googleapis.com/v1/mediaItems:search'
     - Method : 'POST'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */

    func mediaItems(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {
        APIManager.shared.post(url: KEYS.url.googleMediaItem,
                              headers: headers,
                              parameters: parameters,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
    
    /**
     - URL : 'https://accounts.google.com/o/oauth2/token'
     - Method : 'POST'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */
    func refreshAccessTokenAndFetchAlbums(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {
        APIManager.shared.post(url: KEYS.url.googleToken,
                              headers: headers,
                              parameters: parameters,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
    
    /**
     - URL : '"https://api.instagram.com/oauth/access_token"'
     - Method : 'POST'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */
    func instagramAccessToken(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        APIManager.shared.post(url: KEYS.url.instagramAccessToken,
                              headers: _headers,
                              parameters: parameters,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
    
    /**
     - URL : "https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=\(INSTAGRAM_IDS.INSTAGRAM_CLIENTSERCRET)&access_token=\(accessToken)"
     - Method : 'POST'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */
    func instagramLongLiveAccessToken(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let url = "https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=\(instagramClientSecret)&access_token=\(parameters?[KEYS.apiParams._accessToken] ?? "")"
        APIManager.shared.get(url: url,
                              headers: _headers,
                              parameters: nil,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
    
    /**
     - URL : "https://graph.instagram.com/me/media?fields=id,media_type,caption&access_token=\(accessToken)"
     - Method : 'POST'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */
    func instagramFetchPhotos(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {
        var url  = "https://graph.instagram.com/me/media?fields=id,media_type,caption&access_token=\(parameters?[KEYS.apiParams._accessToken] ?? "")"
        if parameters?["nextToken"] as? String != "" {
            url  = "https://graph.instagram.com/me/media?fields=id,media_type,caption&access_token=\(parameters?[KEYS.apiParams._accessToken] ?? "")&after=\(parameters?["nextToken"] ?? "")"
        }
        

        APIManager.shared.get(url: url,
                              headers: nil,
                              parameters: nil,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
    
    /**
     - URL : "https://graph.instagram.com/me/media?fields=id,media_type,caption&access_token=\(accessToken)"
     - Method : 'POST'
     - parameters : -
     - successCallback :
     Object of `AnyObject`
     */
    func instagramFetchPhotosById(parameters:Parameters?=nil,
               headers:HTTPHeaders?=nil,
               completionCallback:@escaping (AnyObject) -> Void,
               success successCallback: @escaping (AnyObject) -> Void,
               failure failureCallback: @escaping (String?) -> Void) {

        let instagramAccessToken = UserDefaults.getInstagramAccessToken()
        let urlString = (parameters?["type"]) as? String == "IMAGE" ? "https://graph.instagram.com/\(parameters?["id"] ?? "")?fields=id,media_type,media_url,username,timestamp&access_token=\(instagramAccessToken ?? "")" : "https://graph.instagram.com/\(parameters?["id"] ?? "")/children?fields=id,media_type,media_url,username,timestamp&access_token=\(instagramAccessToken ?? "")"
        
        APIManager.shared.get(url: urlString,
                              headers: nil,
                              parameters: nil,
                              completionCallback: completionCallback,
                              success: successCallback,
                              failure: failureCallback)
    }
}
