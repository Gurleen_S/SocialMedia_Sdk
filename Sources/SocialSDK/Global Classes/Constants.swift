//
//  Constants.swift
//  
//
//  Created by iOS Developer on 12/01/23.
//

import UIKit
let empty                           = ""
let googleClientId                  = "775198682777-dg8p8gg76ru4jr4i2ndoc5eau5h6md5u.apps.googleusercontent.com"
let instagramClientId               = "215102393043575"
let instagramClientSecret           = "7a77d555773a85aad71f1e6e4542dbb4"

class Constants: NSObject {
    
}
class KEYS {
    static let apiParams = _ApiParamKeys.self
    static let local = _LocalKeys.self
    static let userdefaultkeys = _UserDefaultsKeys.self
    static let url = _URLs.self
}

struct INSTAGRAM_IDS {
    // Snaptouch credentials
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    static let INSTAGRAM_SCOPE = "user_profile,user_media"
}

class _ApiParamKeys {
    static let data                 = "data"
    static let images               = "images"
    static let source               = "source"
    static let paging               = "paging"
    static let cursors              = "cursors"
    static let after                = "after"
    static let name                 = "name"
    static let count                = "count"
    static let picture              = "picture"
    static let url                  = "url"
    static let baseUrl              = "baseUrl"
    static let error                = "error"
    static let albums               = "albums"
    static let coverPhotoBaseUrl    = "coverPhotoBaseUrl"
    static let mediaItemCount       = "mediaItemsCount"
    static let mediaItems           = "mediaItems"
    static let mediaMetaData        = "mediaMetadata"
    static let metaData             = "metaData"
    static let height               = "height"
    static let width                = "width"
    static let nextPageToken        = "nextPageToken"
    static let clientId             = "client_id"
    static let refreshToken         = "refresh_token"
    static let grantType            = "grant_type"
    static let accessToken          = "access_token"
    static let mediaUrl             = "media_url"
    static let mediaType            = "media_type"
    static let _accessToken          = "accessToken"
}

class _LocalKeys {
    // Facebook Permission Keys
    static let email                = "email"
    static let publicProfile        = "public_profile"
    static let userPhotos           = "user_photos"

    //Facebook Graph Api keys
    static let pathKey              = "me"
    static let fieldValue           = "first_name, last_name, email"
    static let fieldKey             = "fields"

    //Facebook user info keys
    static let firstName            = "first_name"
    static let _ids                 = "id"
    static let all                  = "All"
    static let title                = "title"
    static let contentType          = "Content-Type"
    static let authorization        = "Authorization"
    static let applicationJson      = "application/json"

    static let pageSize             = "pageSize"
    static let pageSizeCount        = "100"
    static let albumId              = "albumId"
    static let pageToken            = "pageToken"
    
    //Instagra, user info keys
    static let instragramScope      = "user_profile,user_media"

}

class _UserDefaultsKeys {
    static let facebookUserInfo     = "facebookUserName"
    static let facebookPhotos       = "facebookPhotos"
    static let isFacebookLogin      = "isFacebookLogin"
    
    static let isGoogleLogin        = "isGoogleLogin"
    static let googleUserInfo       = "googleUserName"
    static let googlePhotos         = "googlePhotos"
    static let googleAccessToken    = "googleAccessToken"
    static let googleRefreshToken   = "googleRefreshToken"
    
    static let isInstagramLogin     = "isInstagramLogin"
    static let instagramPhotos      = "instagramImageArray"
    static let instagramAccessToken = "instagramAccessToken"
    static let isLoggedInToInstagram = "isLoggedInToInstagram"
}

class _URLs {
    static let googlePhotoLibrary   = "https://www.googleapis.com/auth/photoslibrary"
    static let googlePhotoAlbum     = "https://photoslibrary.googleapis.com/v1/albums"
    static let googleToken          = "https://accounts.google.com/o/oauth2/token"
    static let googleMediaItem      = "https://photoslibrary.googleapis.com/v1/mediaItems:search"
    static let instagramAuth        = "https://api.instagram.com/oauth/authorize/"
    static let instagramApiUrl      = "https://api.instagram.com/v1/users/"
    static let instagramRedirectUrl = "https://meetpolaroid.com/"
    static let instagramAccessToken = "https://api.instagram.com/oauth/access_token"

}
