//
//  Extension.swift
//  
//
//  Created by iOS Developer on 12/01/23.
//

import UIKit
import IHProgressHUD

class Extension: UIViewController {
}

extension UIViewController {
    
    func showProgrees() {
        IHProgressHUD.show()
    }
    
    func hideProgressHUD() {
        IHProgressHUD.dismiss()
    }
}

extension UIView {
    
    func showProgrees() {
        IHProgressHUD.show()
    }
    
    func hideProgressHUD() {
        IHProgressHUD.dismiss()
    }
}

