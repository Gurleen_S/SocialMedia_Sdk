//
//  UserDefaultExtension.swift
//  
//
//  Created by iOS Developer on 12/01/23.
//

import UIKit
import GoogleSignIn

public func getArchived(data:Any) -> Data {
    var encodedData = Data()

    do {
        if let data = data as? String {
            encodedData = try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false)
        } else if let data = data as? Int {
            encodedData = try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false)
        } else if let data = data as? Bool {
            encodedData = try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false)
        } else if let data = data as? [[String:Any]] {
            encodedData = try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false)
        }
        return encodedData
        
    } catch {
    }
    
    return encodedData
}

public func getUnArchived(data:Data?) -> Any? {
    
    do {
        if data != nil {
            let decodedData =  try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data!)
            return decodedData
        }
    } catch {
    }
    
    return nil
}

public extension UserDefaults {
    
    static func setData(_ value: [[String:Any]],key:String) {
        standard.set(getArchived(data: value as Any), forKey: key)
        standard.synchronize()
    }
    
    static func getData(key:String) -> [[String:Any]]? {
        if let value =  getUnArchived(data: standard.value(forKey: key) as? Data) {
            return (value as! [[String : Any]])
         }
        return nil
    }
    
    static func saveConsentStatusFor(key:String) {
        setValue(true, forKey: key)
    }
    
    static func getConsentStatusFor(key:String) -> Bool {
        return getStatus(forKey:key)
    }

    
}

public extension UserDefaults {
    
    static func getValue<T>(_ forKey:String) -> T? {
        return standard.value(forKey: forKey) as? T
    }
    
    static func getStatus(forKey:String) -> Bool {
        return standard.bool(forKey: forKey)
    }
    
    static func setValue<T>(_ value:T?, forKey:String) {
        standard.set(value, forKey: forKey)
        standard.synchronize()
    }
    
    static func setsatus<T>(_ value:T?, forKey:String) {
        standard.set(value, forKey: forKey)
        standard.synchronize()
    }
    
    static func setString(_ text:String?, forKey:String) {
        setValue(text, forKey: forKey)
    }
    static func remove(_ forKey:String) {
        standard.removeObject(forKey: forKey)
        standard.synchronize()
    }
    
    //-- Get Values
    static func getFacebookUserInfo() -> [String:Any]? {
        return getValue(KEYS.userdefaultkeys.facebookUserInfo)
    }
    
    static func getFacebookLogin() -> Bool? {
        return getValue(KEYS.userdefaultkeys.isFacebookLogin)
    }
    
    static func getFacebookPhotos() -> [String]? {
        return getValue(KEYS.userdefaultkeys.facebookPhotos)
    }
    
    static func getGoogleLogin() -> Bool? {
        return getValue(KEYS.userdefaultkeys.isGoogleLogin)
    }
    
    static func getGoogleUserInfo() -> String? {
        return getValue(KEYS.userdefaultkeys.googleUserInfo)
    }
    
    static func getGooglePhoto() -> GIDSignInResult? {
        return getValue(KEYS.userdefaultkeys.googlePhotos)
    }
    
    static func getGoogleAccessToken() -> String? {
        return getValue(KEYS.userdefaultkeys.googleAccessToken)
    }

    static func getGoogleRefreshToken() -> String? {
        return getValue(KEYS.userdefaultkeys.googleRefreshToken)
    }

    static func getInstagramLogin() -> Bool? {
        return getValue(KEYS.userdefaultkeys.isInstagramLogin)
    }

    static func getInstagramPhotos() -> [String]? {
        return getValue(KEYS.userdefaultkeys.instagramPhotos)
    }
    
    static func getInstagramAccessToken() -> String? {
        return getValue(KEYS.userdefaultkeys.instagramAccessToken)
    }

    //-- Set Values
    static func setFacebookUserInfo(_ value:[String:Any]?) {
        setValue(value, forKey: KEYS.userdefaultkeys.facebookUserInfo)
    }
    
    static func setFacebookLogin(_ value:Bool?) {
        setValue(value, forKey: KEYS.userdefaultkeys.isFacebookLogin)
    }
    
    static func setFacebookPhotos(_ value:[String]) {
        setValue(value, forKey: KEYS.userdefaultkeys.facebookPhotos)
    }
    
    static func setGoogleLogin(_ value:Bool?) {
        setValue(value, forKey: KEYS.userdefaultkeys.isGoogleLogin)
    }

    static func setGoogleUserInfo(_ value:String?) {
        setValue(value, forKey: KEYS.userdefaultkeys.googleUserInfo)
    }
    
    static func setGooglePhoto(_ value:GIDSignInResult?) {
        setValue(value, forKey: KEYS.userdefaultkeys.googlePhotos)
    }
    
    static func setGoogleAccessToken(_ value:String?) {
        setValue(value, forKey: KEYS.userdefaultkeys.googleAccessToken)
    }
    
    static func setGoogleRefreshToken(_ value:String?) {
        setValue(value, forKey: KEYS.userdefaultkeys.googleRefreshToken)
    }
    
    static func setInstagramLogin(_ value:Bool?) {
        setValue(value, forKey: KEYS.userdefaultkeys.isInstagramLogin)
    }

    static func setInstagramArray(_ value:[String]?) {
        setValue(value, forKey: KEYS.userdefaultkeys.instagramPhotos)
    }

    static func setInstagramAccessToken(_ value:String?) {
        setValue(value, forKey: KEYS.userdefaultkeys.instagramAccessToken)
    }

    //-- Remove Values
    static func removeFacebookUserInfo() {
        remove(KEYS.userdefaultkeys.facebookUserInfo)
    }
    
    static func removeFacebookLogin() {
        remove(KEYS.userdefaultkeys.isFacebookLogin)
    }
    
    static func removeFacebookPhotos() {
        remove(KEYS.userdefaultkeys.facebookPhotos)
    }
    
    static func removeGoogleLogin() {
        remove(KEYS.userdefaultkeys.isGoogleLogin)
    }
    
    static func removeGoogleUserInfo() {
        remove(KEYS.userdefaultkeys.googleUserInfo)
    }
    
    static func removeGooglePhoto() {
        remove(KEYS.userdefaultkeys.googlePhotos)
    }
    
    static func removeGoogleAccessToken() {
        remove(KEYS.userdefaultkeys.googleAccessToken)
    }

    static func removeInstagramLogin() {
        remove(KEYS.userdefaultkeys.isInstagramLogin)
    }
    
    static func removeInstagramPhotos() {
        remove(KEYS.userdefaultkeys.instagramPhotos)
    }
    
    static func removeInstagramAccessToken() {
        remove(KEYS.userdefaultkeys.instagramAccessToken)
    }
}
