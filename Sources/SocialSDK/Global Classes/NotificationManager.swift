//
//  NotificationManager.swift
//  
//
//  Created by iOS Developer on 12/01/23.
//

import UIKit

struct NotificationIdentifier {
    static var logout = "sdk.facebook.logout"
    static var login = "sdk.facebook.login"
    static var facebookUserInfo = "sdk.facebook.userinfo"
    static var facebookUserUpdate = "sdk.facebook.userUpdate"
}

class NotificationManager: NSObject {
    static let shared = NotificationManager()
    
    var token : String = ""
    
    private override init() {
        super.init()
    }
}
extension NotificationManager {
    func postFacebookLogout(object: Any? = nil) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationIdentifier.logout), object: object)
    }
    
    func postFacebookUserInfo(object: Any? = nil) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationIdentifier.facebookUserInfo), object: object)
    }
    
    func postFacebookUserUpdate(object: Any? = nil) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationIdentifier.facebookUserUpdate), object: object)
    }
}
