//

import Foundation
import Alamofire

class APIManager {
    
    static let shared = APIManager()
    
    private init() { }
    private lazy var alamoFireManager: Session? = {
       let configuration = URLSessionConfiguration.default
       configuration.timeoutIntervalForRequest = 300
       configuration.timeoutIntervalForResource = 300
        let alamoFireManager = Session(configuration: configuration, startRequestsImmediately: true)
       return alamoFireManager

   }()
    
    func get(url:String, headers:HTTPHeaders?=nil ,parameters:Parameters?=nil,completionCallback:@escaping (AnyObject) -> Void ,success successCallback: @escaping (AnyObject) -> Void ,failure failureCallback: @escaping (String?) -> Void) {
        request(url: url, method: .get, headers: headers, parameters: parameters, completionCallback: completionCallback, success: successCallback, failure: failureCallback)
    }
    
    func post(url:String, headers:HTTPHeaders?=nil,parameters:Parameters?=nil,completionCallback:@escaping (AnyObject) -> Void ,success successCallback: @escaping (AnyObject) -> Void ,failure failureCallback: @escaping (String?) -> Void) {
        request(url: url, method: .post, headers: headers, parameters: parameters, completionCallback: completionCallback, success: successCallback, failure: failureCallback)
    }
    
    func request(url:String,method:HTTPMethod, headers:HTTPHeaders?=nil, parameters:Parameters?=nil,completionCallback:@escaping (AnyObject) -> Void ,success successCallback: @escaping (AnyObject) -> Void ,failure failureCallback: @escaping (String?) -> Void) {
        
        URLCache.shared.removeAllCachedResponses()
        
        var encoding:ParameterEncoding!
        if url == KEYS.url.googleMediaItem {
            encoding = JSONEncoding.default
        } else {
            encoding = URLEncoding.httpBody
        }
        
        alamoFireManager?.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseData { (response) in
            completionCallback(response as AnyObject)
            switch response.result {
            case .success(let data):
                do {
                    let json = try JSONSerialization.jsonObject(with: data)
                    successCallback(json as AnyObject)

                } catch {
                    failureCallback("error")
                }
            case .failure(let error):
                failureCallback(error.errorDescription)
            }
        }
    }
}
