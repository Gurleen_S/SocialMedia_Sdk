//
//  GoogleHelperSignleton.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import GoogleSignIn
import Alamofire


class GoogleHelper: UIView, URLSessionDelegate {
    
    /**
     This method use to login any user to Google
     - Parameters: viewController
     - Returns: -
     */
    
    func loginGoogle(viewController: UIViewController) {
        GoogleHelper().logoutGoogle()
        GIDSignIn.sharedInstance.signIn(withPresenting: viewController, hint: empty, additionalScopes: [KEYS.url.googlePhotoLibrary]) { user,error in
            /// Get the access token to attach it to a REST or gRPC request.
            /// Or, get an object that conforms to GTMFetcherAuthorizationProtocol for
            /// use with GTMAppAuth and the Google APIs client library.
            UserDefaults.setGoogleRefreshToken(user?.user.refreshToken.tokenString)
            UserDefaults.setGoogleAccessToken(user?.user.accessToken.tokenString)
            UserDefaults.setGoogleLogin(true)
            UserDefaults.setGoogleUserInfo(user?.user.profile?.name)
        }
    }
    
    /**
     This method use to logout any user from google
     - Parameters: -
     - Returns: -
     */
    func logoutGoogle() {
        GIDSignIn.sharedInstance.signOut()
        UserDefaults.removeGoogleLogin()
        UserDefaults.removeGoogleUserInfo()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedOut"), object: nil)
    }
    
    /**
     This method use to logout any user from facebook
     - Parameters: -
     - Returns: -
     */
    func getAlbumTitles(accessToken : String, completion: @escaping(_ photos: [GooglePhotosModels], _ isTokenExpired: Bool) -> ()) {
        self.showProgrees()
        ApiActions.googleAlbum(parameters: [KEYS.apiParams._accessToken:accessToken], completionCallback: { (_ ) in
            self.hideProgressHUD()
        }, success: { (data) in
            if data.count == 0 {
                /// If we got nil data , we need fetch photos according to album ids
                self.fetchAlbumDetailsByID(albumId: empty, accessToken: accessToken, nextPageToken: empty) { (googlePhotosArray ,photos, nextPageToken) in
                    let googlePhoto = GooglePhotosModels(dataDict: [], googleAlbumPhotoLinkArray: [], googleAlbumId: empty, coverPhoto: photos.first?[KEYS.apiParams.baseUrl] as? String ?? empty , title: KEYS.local.all, mediaCount: empty)
                    var googlePhotos = [GooglePhotosModels]()
                    googlePhotos.append(googlePhoto)
                    completion(googlePhotos, false)
                }
            } else {
                var googlePhotos = [GooglePhotosModels]()
                if let dictionaryResponse = data[KEYS.apiParams.albums] as? [[String: Any]] {
                    if !dictionaryResponse.isEmpty {
                        if let coverPhoto = dictionaryResponse[0][KEYS.apiParams.coverPhotoBaseUrl] as? String {
                            let paraGoogle = GooglePhotosModels(dataDict: [], googleAlbumPhotoLinkArray: [], googleAlbumId: empty, coverPhoto: coverPhoto, title: KEYS.local.all, mediaCount: empty)
                            googlePhotos.append(paraGoogle)
                        }
                        for data in dictionaryResponse {
                            if let mediaItemsCount = data[KEYS.apiParams.mediaItemCount] as? String, let albumId = data[KEYS.local._ids] as? String, let coverPhoto = data[KEYS.apiParams.coverPhotoBaseUrl] as? String, let title = data[KEYS.local.title] as? String {
                                let paraGoogle = GooglePhotosModels(dataDict: [], googleAlbumPhotoLinkArray: [], googleAlbumId: albumId, coverPhoto: coverPhoto, title: title, mediaCount: mediaItemsCount)
                                googlePhotos.append(paraGoogle)
                            }
                        }
                        completion(googlePhotos, false)
                    } else {
                        completion([], false)
                    }
                } else {
                    completion([], false)
                }
            }
        }) { (error) in
        }
    }
    
    /**
     This method use to get images according album ids
     - Parameters: -
     - Returns: -
     */
    
    func fetchAlbumDetailsByID(albumId : String, accessToken:String, nextPageToken: String, completion: @escaping(_ googlePhotosArray: [String],_ photos: [[String: Any]], _ nextPageToken: String) -> ())
    {
        let album_id = albumId
        var arrayAllPhotos = [[String: Any]]()
        var arrayGooglePhotos = [String]()
        let headers : HTTPHeaders = [
            KEYS.local.contentType:KEYS.local.applicationJson,
            KEYS.local.authorization : "Bearer \(accessToken)"
        ]
        var params : Parameters = [KEYS.local.pageSize: KEYS.local.pageSizeCount, KEYS.local.albumId: album_id]
        if !nextPageToken.isEmpty {
            params[KEYS.local.pageToken] = nextPageToken
        }
        
        ApiActions.mediaItems(parameters: params,headers: headers,completionCallback: { (_ ) in
            self.hideProgressHUD()
        }, success: { data in
            let dict = data as! NSDictionary
            guard let dictionaryResponse = dict.value(forKey: KEYS.apiParams.mediaItems) as? NSArray
            else {
                return
            }
            arrayAllPhotos.removeAll()
            if let arrayLocallyAlbum = dictionaryResponse as? [[String:Any]] {
                for dict in arrayLocallyAlbum {
                    guard let mediaUrl = dict[KEYS.apiParams.baseUrl] as? String, let mediaMetaData = dict[KEYS.apiParams.mediaMetaData] as? [String: Any] else {
                        completion([],[], empty)
                        return
                    }
                    var dict = [String: Any]()
                    if let height = mediaMetaData[KEYS.apiParams.height] as? String, let width = mediaMetaData[KEYS.apiParams.width] as? String {
                        dict[KEYS.apiParams.metaData] = "=w\(width)-h\(height)"
                        dict[KEYS.apiParams.baseUrl] = mediaUrl
                    }
                    arrayAllPhotos.append(dict)
                    arrayGooglePhotos.append(dict[KEYS.apiParams.baseUrl] as! String)
                }
                if let nextPageToken = dict[KEYS.apiParams.nextPageToken] as? String {
                    completion(arrayGooglePhotos,arrayAllPhotos, nextPageToken)
                } else {
                    completion(arrayGooglePhotos,arrayAllPhotos, empty)
                }
            }
        }) { (error) in
            
        }
    }
    
    /**
     This method use to Refresh token
     - Parameters: -
     - Returns: -
     */
    func refreshAccessTokenAndFetchAlbums(completion: @escaping(_ token: String) -> () = {_ in}) {
        let parameters = [KEYS.apiParams.clientId: googleClientId, KEYS.apiParams.refreshToken: UserDefaults.getGoogleRefreshToken() ?? "", KEYS.apiParams.grantType:KEYS.apiParams.refreshToken] as [String : Any]
        
        ApiActions.refreshAccessTokenAndFetchAlbums(parameters: parameters, headers:nil, completionCallback: { (_ ) in
            self.hideProgressHUD()
        }, success: { (jsonResult) in
            guard let accessToken = jsonResult[KEYS.apiParams.accessToken] as? String else {
                return
            }
            completion(accessToken)
            UserDefaults.setGoogleAccessToken(accessToken)
            UserDefaults.setGoogleLogin(true)
        }) { (error) in
            NotificationCenter.default.post(name: Notification.Name(rawValue: "googleRefreshTokenExpired"), object: nil)
        }
    }
}
