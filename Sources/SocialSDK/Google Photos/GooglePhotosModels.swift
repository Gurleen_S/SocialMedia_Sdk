//
//  GooglePhotosPara.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import ShipBookSDK

public struct GooglePhotosModels {
    public var dataDict = [[String:Any]]()
    public var googleAlbumPhotoLinkArray = [String]()
    public var googleAlbumId = String()
    public var coverPhoto = String()
    public var title = String()
    public var mediaCount = String()
}
