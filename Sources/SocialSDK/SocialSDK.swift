//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import GoogleSignIn

public struct SocialSDK {
       
    static let shared = SocialSDK()
    
    public init() {}
    
    //-- call this method to login or fetch photos in instagram
    public func socialMediaInstagram(viewController: UIViewController) {
        InstagramHelper().loginInstagram(viewController: viewController)
    }
    
    //-- call this array method to get photos array
    public func getInstagramImageFromDB() -> [String]
    {
        if let photos = UserDefaults.getInstagramPhotos(), photos.count != 0 {
            return photos
        }
        return []
    }
    
    //-- call this method to login or fetch photos from google photos
    public func socialMediaGooglePhotos(viewController: UIViewController, completion: @escaping(_ googlePhotos: [GooglePhotosModels]) -> () = {_ in}) {
        if UserDefaults.getGoogleLogin() == true {
            let accessToken = UserDefaults.getGoogleAccessToken() ?? ""
            GoogleHelper().getAlbumTitles(accessToken: accessToken) { (googlePhotos, isTokenExpired) in
                if isTokenExpired {
                    GoogleHelper().refreshAccessTokenAndFetchAlbums() { (token) in
                        GoogleHelper().getAlbumTitles(accessToken: token) { (phtos, isTokenExpired) in
                            completion(googlePhotos)
                        }
                    }
                } else {
                    completion(googlePhotos)
                }
            }
        } else {
            GoogleHelper().loginGoogle(viewController: viewController)
        }
    }
    
    //
    public func getGooglePhotosFromID(viewController: UIViewController, selectedAlbumId : String, accessToken:String, nextPageToken: String, completion: @escaping(_ googlePhotosArray: [String],_ photos: [[String: Any]], _ nextPageToken: String) -> () = {_,_,_   in}) {
        GoogleHelper().fetchAlbumDetailsByID(albumId: selectedAlbumId, accessToken: accessToken, nextPageToken: nextPageToken) { googlePhotosArray,photos,nextPageToken in
            completion(googlePhotosArray,photos,nextPageToken)
        }
    }
    
    
    //-- call this method to login or fetch photos from facebook
    public func fetchSocialMediaFacebook(completion: @escaping(_ arrayFBAlbum: [FacebookModels]) -> () = {_ in}) {
        FacebookHelper().fetchAlbumFacebook() { arrayFBAlbum, success in
            if success && arrayFBAlbum.count > 0 {
                completion(arrayFBAlbum)
            } else {
                FacebookHelper().loginFacebook()
            }
        }
    }
    
    public func getFacebookPhotosFromID(viewController: UIViewController, album: FacebookModels, completion: @escaping(_ Result: Bool,_ pageToken:String,_ arrayPhoto: [String]) -> () = {_,_,_   in}) {
        FacebookHelper().fetchAlbumDetailsByID(viewController: viewController, facebookObject: album,pageToken : "", showLoader: true) { result, pageToken,arrayPhoto  in
            completion(result,pageToken, arrayPhoto)
        }
    }
}
