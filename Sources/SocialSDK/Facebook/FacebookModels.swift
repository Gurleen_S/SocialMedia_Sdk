//
//  FacebookAlbum_Para.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import ShipBookSDK

public class FacebookModels: NSObject
{
    public var stringAlbumName = String()
    public var intPhotosCount = Int()
    public var stringAlbumId = String()
    public var arrayAlbumPhotoLink = [String]()
    public var stringAlbumThumbnailUrl = String()
}
