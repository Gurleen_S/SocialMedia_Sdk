//
//  FacebookHelper.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import ShipBookSDK
import FBSDKCoreKit
import FBSDKLoginKit

class FacebookHelper: UIView {
    
    let dispatchQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    
    /**
     This method use to login any user to facebook
     - Parameters: -
     - Returns: -
     */
    
    func loginFacebook() {
        
        /// Removing all cookies , all session and userdefaults values
        self.logoutFacebook()
        
        let loginManager = LoginManager()
        
        self.showProgrees()
        /// Logs the user in or authorizes additional permissions.
        loginManager.logIn(permissions: [KEYS.local.email,KEYS.local.publicProfile,KEYS.local.userPhotos], from: nil) {
            (result, error) -> Void in
            
            /// if we have an error display it and abort
            if error != nil {
                self.hideProgressHUD()
                return
            }
            
            /// Make sure if we have a result, otherwise abort
            guard let result = result else { return }
            
            /// if cancelled nothing todo
            if result.isCancelled {
                self.hideProgressHUD()
                return
            } else {
                /// Calling method to fetch user info of logined user
                self.fetchUserInfoFacebook()
                UserDefaults.setFacebookLogin(true)
            }
        }
    }
    
    /**
     This method use to logout any user from facebook
     - Parameters: -
     - Returns: -
     */
    
    func logoutFacebook() {
        /// `FBSDKLoginManager` provides methods for logging the user in and out.
        /// `FBSDKLoginManager` serves to help manage sessions represented by tokens for authentication,
        let loginManager = LoginManager()
        
        /// Clear cookies
        self.clearCookiesFacebook()
        
        /// Added this so that after logout it asks you to enter new credentials.
        loginManager.logOut()
        
        /// Removing access token initially from `FBSDKAccessToken`
        AccessToken.current = nil
        
        /// Removing facebook profile session from `FBSDKProfile`
        Profile.current = nil
        
        UserDefaults.setFacebookLogin(false)
        UserDefaults.removeFacebookUserInfo()
        NotificationManager.shared.postFacebookLogout()
    }
    
    /**
     This method use to clear cookies of facebook from domain
     - Parameters: -
     - Returns: -
     */
    
    func clearCookiesFacebook() {
        
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            let domain = cookie.domain
            if domain == ".facebook.com" {
                storage.deleteCookie(cookie)
            }
        }
    }
    
    
    /**
     This method use to fetch details like first name, last name and email.
     Represents a request to the Facebook Graph API.
     `FBSDKGraphRequest` encapsulates the components of a request (the
     Graph API path, the parameters, error recovery behavior) and should be
     used in conjunction with `FBSDKGraphRequestConnection` to issue the request.
     
     - Parameters: -
     - Returns: -
     */
    
    func fetchUserInfoFacebook()
    {
        if AccessToken.current != nil {
            
            GraphRequest(graphPath: KEYS.local.pathKey, parameters: [KEYS.local.fieldKey : KEYS.local.fieldValue]).start() {
                (_, result, error) in
                self.hideProgressHUD()
                
                /// if we have an error display it and abort
                if error != nil {
                    return
                }
                
                /// parse the fields out of the result
                if let fields = result as? [String:Any] {
                    UserDefaults.setFacebookUserInfo(fields)
                    NotificationManager.shared.postFacebookUserInfo()
                }
            }
        }
    }
    
    /**
     This method will fetch photos from album of google photos
     - Parameters: -
     - Returns: -
     */
    
    func fetchAlbumFacebook(completion: @escaping(_ arrayFBAlbum: [FacebookModels], _ success: Bool) -> () = {_,_  in})
    {
        // For more complex open graph stories, use `FBSDKShareAPI`
        // with `FBSDKShareOpenGraphContent`
        /* make the API call */
        
        if AccessToken.current != nil {
            self.showProgrees()
            let graphRequest : GraphRequest = GraphRequest(graphPath: "/me/albums?fields=count,id,name,picture&limit=1000", parameters: [:] )
            graphRequest.start{ (connection, result, error) -> Void in
                if error != nil {
                    DispatchQueue.main.async {
                        self.hideProgressHUD()
                        completion([], false)
                    }
                    return
                }
                
                guard let resultDict = result as? [String:Any] else {
                    completion([], false)
                    DispatchQueue.main.async {
                        self.hideProgressHUD()
                        completion([], false)
                    }
                    return
                }
                
                var facebookObjects = [FacebookModels]()
                if let arrayAlbum = resultDict[KEYS.apiParams.data] as? [[String:Any]] {
                    facebookObjects = arrayAlbum.map({ album in
                        let fbObject = FacebookModels()
                        fbObject.stringAlbumId = album[KEYS.local._ids] as! String
                        fbObject.stringAlbumName = album[KEYS.apiParams.name] as! String
                        fbObject.intPhotosCount = album[KEYS.apiParams.count] as! Int
                        
                        let dictionaryPicture = album[KEYS.apiParams.picture] as? [String:Any]
                        if let dataDict = dictionaryPicture![KEYS.apiParams.data] as? [String:Any], let url = dataDict[KEYS.apiParams.url] as? String {
                            fbObject.stringAlbumThumbnailUrl = url
                        }
                        return fbObject
                    })
                }
                
                completion(facebookObjects, true)
                self.hideProgressHUD()
            }
        } else {
            self.hideProgressHUD()
            completion([], false)
        }
    }
    
    /**
     This method will fetch photos by album ids of google photos
     - Parameters: -
     - Returns: -
     */
    func fetchAlbumDetailsByID(viewController : UIViewController, facebookObject : FacebookModels, pageToken : String, showLoader: Bool, completion: @escaping(_ Result: Bool, _ PageToken:String, _ arrayPhoto: [String]) -> () = {_,_,_  in}) {
        
        self.showProgrees()
        let albumId = facebookObject.stringAlbumId
        
        let graphRequest : GraphRequest = GraphRequest(graphPath: "/\(albumId)/photos?fields=id,picture,images,album&limit=100&after=\(pageToken)", parameters: [:] )
        graphRequest.start { (connection, result, error) -> Void in
            if let resultDict = result as? [String:Any] {
                self.allPhotosFromFacebook(result: resultDict, facebookObject: facebookObject) { result, pageToken, arrayPhoto in
                    completion(result, pageToken, arrayPhoto)
                }
            } else {
                self.hideProgressHUD()
            }
        }
    }
    
    
    func allPhotosFromFacebook(result: [String:Any], facebookObject : FacebookModels, completion: @escaping(_ result: Bool, _ pageToken:String, _ arrayPhoto: [String]) -> () = {_,_,_  in}) {
        
        /// To add photos from facebook
        var arrayPhotos = [String]()
        var stringNextPageToken = String()
        self.dispatchQueue.sync() { () -> Void in
            var arrayPhotoLink = [String]()
            if let dataArray = result[KEYS.apiParams.data] as? [[String:Any]] {
                arrayPhotoLink = dataArray.map({ data in
                    if
                        let imagesArray = data[KEYS.apiParams.images] as? [[String:Any]],
                        imagesArray.count > 0,
                        let photoUrl = imagesArray[0][KEYS.apiParams.source] as? String {
                        return photoUrl
                    }
                    return empty
                })
            }
            
            if let paging = result[KEYS.apiParams.paging] as? [String:Any] {
                if let pagekey = paging[KEYS.apiParams.cursors] as? [String:Any] {
                    stringNextPageToken = pagekey[KEYS.apiParams.after] as? String ?? empty
                }
            }
            facebookObject.arrayAlbumPhotoLink = arrayPhotoLink
        }
        
        arrayPhotos.append(contentsOf: facebookObject.arrayAlbumPhotoLink)
        self.hideProgressHUD()
        completion(true,stringNextPageToken, arrayPhotos)
        UserDefaults.setFacebookPhotos(arrayPhotos)
        NotificationManager.shared.postFacebookUserUpdate()
    }
}
