//
// InstagramViewController.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//


import UIKit
import ShipBookSDK
import Alamofire
import WebKit

class InstagramViewController: UIViewController {
    
    @IBOutlet weak var webViewLogin: WKWebView!
    var stringLongLiveAccesstoken = empty
    var arrayInstagramImages = [String]()
    var arrayNewIds = [String]()
    let dispatchGroup = DispatchGroup()
    let dispatchQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    var insatagramArray = [String]()
    var callback: ((_ Cancel: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialLoad()
    }
}

extension InstagramViewController {
    func initialLoad() {
        self.webViewLogin.uiDelegate = self
        self.webViewLogin.navigationDelegate = self
        self.unsignedRequest()
    }
}

//MARK: - IBActions
extension InstagramViewController {
    @IBAction func doneBtnClicked(_ sender: Any){
        callback?(true)
        self.dismiss(animated: true)
    }
}

// MARK: - WKNavigationDelegate
extension InstagramViewController: WKUIDelegate,WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showProgrees()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideProgressHUD()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let _ = self.checkRequestForCallbackURL(request: navigationAction.request)
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
    }
}

extension InstagramViewController
{
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(KEYS.url.instagramRedirectUrl) {
            if let range: Range<String.Index> = requestURLString.range(of: "?code=") {
                let replaced = requestURLString[range.upperBound...].dropLast(2)
                self.getAccessToken(code: String(replaced))
            }
            return false;
        }
        return true
    }
    
    func getAccessToken(code: String) {
        let params : Parameters = [KEYS.apiParams.clientId: "\(instagramClientId)",
                                   "client_secret": "\(instagramClientSecret)",
                                   KEYS.apiParams.grantType: "authorization_code",
                                   "redirect_uri": "\(KEYS.url.instagramRedirectUrl)",
                                   "code": "\(code)"]
        ApiActions.instagramAccessToken(parameters: params, headers:nil, completionCallback: { (_ ) in
        }, success: { (jsonResult) in
            var stringAccessToken : String?
            if jsonResult[KEYS.apiParams.accessToken] != nil {
                stringAccessToken = jsonResult[KEYS.apiParams.accessToken] as? String ?? ""
            }
            UserDefaults.setInstagramLogin(true)
            self.getLongLivedAccessToken(accessToken: stringAccessToken ?? "")
        }) { (error) in
        }
    }
    
    func getLongLivedAccessToken(accessToken: String){
        ApiActions.instagramLongLiveAccessToken(parameters: [KEYS.apiParams._accessToken: accessToken ], headers:nil, completionCallback: { (_ ) in
        }, success: { (jsonResponse) in
            if jsonResponse[KEYS.apiParams.accessToken] != nil
            {
                guard let accessToken = jsonResponse[KEYS.apiParams.accessToken] as? String else{
                    return
                }
                self.stringLongLiveAccesstoken = accessToken
                UserDefaults.setInstagramAccessToken(self.stringLongLiveAccesstoken)
                self.fetchPhotosInstagram(accessToken: accessToken)
                self.webViewLogin.isHidden = true
            }
            
        }) { (error) in
        }
    }
    
    func fetchPhotosInstagram(accessToken: String, nextToken:String?=nil) {
        let params : Parameters = [KEYS.apiParams._accessToken: accessToken, "nextToken": nextToken ?? ""]
        ApiActions.instagramFetchPhotos(parameters: params, headers:nil, completionCallback: { (_ ) in
        }, success: { (jsonResult) in
            if let jsonResultData = jsonResult[KEYS.apiParams.data] as? [[String:Any]] {
                for jsonDict in jsonResultData {
                    self.dispatchGroup.enter()
                    
                    if let id = jsonDict[KEYS.local._ids] as? String, let type = jsonDict[KEYS.apiParams.mediaType] as? String {
                        self.dispatchQueue.sync() { () -> Void in
                            self.fetchPhotosFromAlbumWithID(id:id, type: type)
                        }
                    }
                }
            }
            
            if let pagination = jsonResult[KEYS.apiParams.paging] as? [String:Any] {
                if pagination.count > 0 {
                    if let pageDict = pagination[KEYS.apiParams.cursors] as? [String:Any] {
                        if let next_token = pageDict[KEYS.apiParams.after] as? String {
                            let next_url = "&after=\(next_token)"
                            self.fetchPhotosInstagram(accessToken: accessToken, nextToken: next_url)
                            return
                        }
                    }
                }
            }
            
            self.dispatchGroup.notify(queue: .main) {
                self.saveImagesInDBAndMoveToNext()
            }
            
        }) { (error) in
        }
    }
    
    func saveImagesInDBAndMoveToNext() {
        self.hideProgressHUD()
        if self.insatagramArray.count > 0 {
            UserDefaults.setInstagramArray(self.insatagramArray)
            self.insatagramArray.removeAll()
        }
        self.dismiss(animated: true) {
        }
    }
    
    func fetchPhotosFromAlbumWithID(id: String, type:String = empty) {
        let params : Parameters = ["id": id , "type": type ]
        ApiActions.instagramFetchPhotosById(parameters: params, headers:nil, completionCallback: { (_ ) in
        }, success: { (jsonResult) in
            self.dispatchQueue.sync() { () -> Void in
                if let imageData = jsonResult[KEYS.apiParams.data] as? [[String:Any]] {
                    for jsonDict in imageData {
                        if let media_url = jsonDict[KEYS.apiParams.mediaUrl] as? String {
                            self.insatagramArray.append(media_url)
                        }
                    }
                }
                if let jsonDict = jsonResult as? [String:Any] {
                    if let media_url = jsonDict[KEYS.apiParams.mediaUrl] as? String {
                        self.insatagramArray.append(media_url)
                    }
                }
            }
            self.dispatchQueue.sync() { () -> Void in
                self.dispatchGroup.leave()
            }

        }) { (error) in
        }
    }
    
    func unsignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code&scope=%@", arguments: [KEYS.url.instagramAuth,instagramClientId,KEYS.url.instagramRedirectUrl, KEYS.local.instragramScope])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webViewLogin.load(urlRequest)
    }
}
