//
//  InstagramHelper.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import ShipBookSDK
import WebKit

class InstagramHelper: NSObject
{
    func loginInstagram(viewController : UIViewController,completion: @escaping(_ PressCancel: Bool) -> () = {_ in}) {
        self.logoutInstagram()
        let _viewController = viewController.storyboard?.instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
        _viewController.callback = { (cancel) -> Void in
            completion(cancel)
        }
        _viewController.modalPresentationStyle = .fullScreen
        viewController.present(_viewController, animated: true)
    }
    
    func logoutInstagram()
    {
        self.clearInstagramCookies()
        UserDefaults.setInstagramLogin(false)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "userLoggedOut"), object: nil)
    }
    
    
    func clearInstagramCookies() {
        InstagramHelper.clean()
        let cookieStore = HTTPCookieStorage.shared
        guard let cookies = cookieStore.cookies, cookies.count > 0 else { return }
        
        for cookie in cookies {
            if cookie.domain == "www.instagram.com" || cookie.domain == "api.instagram.com" || cookie.domain == ".instagram.com" {
                cookieStore.deleteCookie(cookie)
            }
        }
    }
    
    // Clear cookie for WKwebkit
    class func clean() {
        guard #available(iOS 9.0, *) else {return}
        
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                #if DEBUG
                #endif
            }
        }
    }
}
