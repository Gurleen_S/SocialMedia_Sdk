# SocialMediaSDK

Social Media is a sdk written in swift.

## Features
- If user want to fetch photos from social media platform. You can use
this sdk to fetch photos from instagram , google photos and facebook.

## TRY IT OUT

### Swift Package Manager (available Xcode 11.2 and forward)

1. In Xcode, select File > Swift Packages > Add Package Dependency.
2. Follow the prompts using the URL for this repository and a minimum semantic version of v1.0
### Swift Package Manager

With [Swift Package Manager](https://swift.org/package-manager), 
add the following `dependency` to your `Package.swift`:

```swift
dependencies: [
    .package(url: "https://gitlab.com/Gurleen_S/SocialMedia_Sdk.git", .upToNextMajor(from: "1.0.0"))
]
```

## Dependencies

- [Facebook-ios-sdk 9.0.0](https://github.com/facebook/facebook-ios-sdk)
- [GoogleSignIn-iOS 7.0.0](https://github.com/google/GoogleSignIn-iOS)
- [ShipbookSDK 1.1.9](https://github.com/ShipBook/ShipBookSDK-iOS.git)
- [Alamofire 5.6.4](https://github.com/Alamofire/Alamofire)
- [AlamofireImage 4.2.0](https://github.com/Alamofire/AlamofireImage.git)
- [IHProgressHUD 0.1.8](https://github.com/Swiftify-Corp/IHProgressHUD.git) 
